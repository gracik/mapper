<?php

declare(strict_types=1);

use Symplify\EasyCodingStandard\Config\ECSConfig;
use Symplify\EasyCodingStandard\ValueObject\Option;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

return static function (ECSConfig  $config): void {
    $config->paths([__DIR__ . '/src', __DIR__ . '/tests']);

    // A. full sets
    $config->import(SetList::PSR_12);
    $config->import(SetList::ARRAY);
    $config->import(SetList::CLEAN_CODE);
    $config->import(SetList::STRICT);

    // B. standalone rule
    $services = $config->services();

    $services->set(\SlevomatCodingStandard\Sniffs\Commenting\DocCommentSpacingSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\Commenting\RequireOneLinePropertyDocCommentSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\ControlStructures\DisallowYodaComparisonSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\ControlStructures\NewWithParenthesesSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\ControlStructures\UselessIfConditionWithReturnSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\Functions\RequireTrailingCommaInCallSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\Functions\RequireTrailingCommaInDeclarationSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\Namespaces\AlphabeticallySortedUsesSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\Namespaces\NamespaceDeclarationSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\Namespaces\UnusedUsesSniff::class)
        ->property('searchAnnotations', true);
    $services->set(\SlevomatCodingStandard\Sniffs\Namespaces\UseSpacingSniff::class)
        ->property('linesCountBeforeFirstUse', 1)
        ->property('linesCountBetweenUseTypes', 1)
        ->property('linesCountAfterLastUse', 1);
    $services->set(\SlevomatCodingStandard\Sniffs\PHP\ShortListSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\PHP\TypeCastSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\PHP\UselessSemicolonSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\TypeHints\DeclareStrictTypesSniff::class)
        ->property('spacesCountAroundEqualsSign', 0);
    $services->set(\SlevomatCodingStandard\Sniffs\TypeHints\DisallowArrayTypeHintSyntaxSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\TypeHints\NullableTypeForNullDefaultValueSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\TypeHints\ParameterTypeHintSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\TypeHints\ParameterTypeHintSpacingSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\TypeHints\PropertyTypeHintSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\TypeHints\ReturnTypeHintSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\TypeHints\ReturnTypeHintSpacingSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\TypeHints\UnionTypeHintFormatSniff::class)
        ->property('shortNullable', 'yes')
        ->property('nullPosition', 'last');
    $services->set(\SlevomatCodingStandard\Sniffs\Variables\UnusedVariableSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\Variables\UselessVariableSniff::class);
    $services->set(\SlevomatCodingStandard\Sniffs\Whitespaces\DuplicateSpacesSniff::class);

    $parameters = $config->parameters();
    $parameters->set(Option::PARALLEL, true);
};
