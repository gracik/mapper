# gracik/mapper

Map dumb arrays to objects.

## Features and TODOs
 - [x] Map empty arrays to empty objects
 - [x] Generate mapper classes for empty classes
 - [ ] Generate mapper classes for classes with primitive fields
 - [ ] Generate mapper classes for classes with fields of any php type (and most of docblock phpstan types)
 - [ ] Generate and validate openapi schema for request/response classes
 - [ ] Generate and validate openapi schema for whole requests from controllers
 - [ ] Provide request bodies and query parameters to controllers

## Release notes
See [CHANGES.md](CHANGES.md)