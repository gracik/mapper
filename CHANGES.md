# Release notes

## 0.0.1 - 12. November 2022
- Create project
- Map empty classes

## 0.0.2 - 27. November 2022
- Generate mapper classes for empty classes
