<?php

declare(strict_types=1);

namespace Gracik\Mapper\Tests\Generator;

use Gracik\Mapper\Generator\ClassMapperGenerator;
use Gracik\Mapper\Tests\TestProject\Person;
use Gracik\Mapper\Type\TypeResolver;
use PHPUnit\Framework\TestCase;

final class ClassMapperGeneratorTest extends TestCase
{
    public function testGenerator(): void
    {
        $generator = new ClassMapperGenerator(
            new TypeResolver(),
        );

        $mapperClass = '\\Gracik\\Mapper\\Tests\\TestProject\\GeneratedMapper\\PersonMapper';
        $class = $generator->generate(Person::class, $mapperClass);

        $file = __DIR__ . '/../TestProject/GeneratedMapper/PersonMapper.php';
        @unlink($file);
        file_put_contents($file, $class);

        self::assertTrue(class_exists($mapperClass));
        @unlink($file);

        $value = [
            'name' => 'John',
            'age' => 24,
            'weight' => 95.2,
            'isAlive' => true,
        ];
        $mapper = new $mapperClass();

        self::assertEquals(
            expected: new Person(
                name: 'John',
                age: 24,
                weight: 95.2,
                isAlive: true,
            ),
            actual: $mapper->map($value)
        );
    }
}
