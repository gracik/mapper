<?php

declare(strict_types=1);

namespace Gracik\Mapper\Tests\TestProject;

final readonly class Person
{
    public function __construct(
        public string $name,
        public int $age,
        public float $weight,
        public bool $isAlive,
    ) {
    }
}
