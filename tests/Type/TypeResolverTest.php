<?php

declare(strict_types=1);

namespace Gracik\Mapper\Tests\Type;

use DateInterval;
use DateTimeImmutable;
use DateTimeZone;
use Gracik\Mapper\Type\ClassType;
use Gracik\Mapper\Type\FloatType;
use Gracik\Mapper\Type\IntegerType;
use Gracik\Mapper\Type\InterfaceType;
use Gracik\Mapper\Type\IntersectionType;
use Gracik\Mapper\Type\NullType;
use Gracik\Mapper\Type\SelfType;
use Gracik\Mapper\Type\StringType;
use Gracik\Mapper\Type\TypeResolver;
use Gracik\Mapper\Type\UnionType;
use Gracik\Mapper\Type\VariadicType;
use PHPUnit\Framework\TestCase;
use ReflectionParameter;
use Serializable;
use Stringable;

class TypeResolverTest extends TestCase
{
    public function testResolveParameter(): void
    {
        $thing = new class() {
            public function setTime(DateTimeImmutable $date): void
            {
            }

            public function add(self $thing, int $priority, string $name): static
            {
                return $this;
            }

            public function addMore(self ...$things): static
            {
                return $this;
            }

            public function addMoreNumbers(int|float ...$things): static
            {
                return $this;
            }

            public function setJsonThing(Stringable&Serializable $jsonThing): static
            {
                return $this;
            }
        };

        $typeResolver = new TypeResolver();

        self::assertEquals(new ClassType(DateInterval::class), $typeResolver->resolveParameter(new ReflectionParameter([DateTimeImmutable::class, 'add'], 0)));
        self::assertEquals(new UnionType(new ClassType(DateTimeZone::class), new NullType()), $typeResolver->resolveParameter(new ReflectionParameter([DateTimeImmutable::class, 'createFromFormat'], 'timezone')));
        self::assertEquals(new SelfType(), $typeResolver->resolveParameter(new ReflectionParameter([$thing, 'add'], 0)));
        self::assertEquals(new IntegerType(), $typeResolver->resolveParameter(new ReflectionParameter([$thing, 'add'], 1)));
        self::assertEquals(new StringType(), $typeResolver->resolveParameter(new ReflectionParameter([$thing, 'add'], 2)));
        self::assertEquals(new VariadicType(new SelfType()), $typeResolver->resolveParameter(new ReflectionParameter([$thing, 'addMore'], 0)));
        self::assertEquals(new VariadicType(new UnionType(new IntegerType(), new FloatType())), $typeResolver->resolveParameter(new ReflectionParameter([$thing, 'addMoreNumbers'], 0)));
        self::assertEquals(new IntersectionType(new InterfaceType(Stringable::class), new InterfaceType(Serializable::class)), $typeResolver->resolveParameter(new ReflectionParameter([$thing, 'setJsonThing'], 0)));
    }
}
