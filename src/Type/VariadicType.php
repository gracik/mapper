<?php

declare(strict_types=1);

namespace Gracik\Mapper\Type;

final class VariadicType implements Type
{
    public function __construct(public readonly Type $type = new MixedType())
    {
    }
    public function toPhpTypeString(): string
    {
        return "...{$this->type->toPhpTypeString()}";
    }

    public function toDocblockTypeString(): string
    {
        return "...{$this->type->toDocblockTypeString()}";
    }

    public function __toString(): string
    {
        return $this->toDocblockTypeString();
    }
}
