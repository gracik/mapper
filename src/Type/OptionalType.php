<?php

declare(strict_types=1);

namespace Gracik\Mapper\Type;

class OptionalType implements Type
{
    public function __construct(
        public readonly Type $type,
    ) {
    }

    public static function create(Type $type): self
    {
        return $type instanceof self ? $type : new self($type);
    }

    public function toPhpTypeString(): string
    {
        return (new UnionType($this->type, new NullType()))->toPhpTypeString();
    }

    public function toDocblockTypeString(): string
    {
        return (new UnionType($this->type, new NullType()))->toDocblockTypeString();
    }

    public function __toString(): string
    {
        return $this->toDocblockTypeString();
    }
}
