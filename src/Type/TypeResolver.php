<?php

declare(strict_types=1);

namespace Gracik\Mapper\Type;

use LogicException;
use ReflectionIntersectionType;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionProperty;
use ReflectionUnionType;

final class TypeResolver
{
    public function resolveParameter(ReflectionParameter $parameter): Type
    {
        /** @var ReflectionNamedType|ReflectionUnionType|ReflectionIntersectionType|null $reflectionType */
        $reflectionType = $parameter->getType();
        return $this->resolveType($reflectionType, $parameter->isVariadic());
    }

    public function resolveProperty(ReflectionProperty $property): Type
    {
        /** @var ReflectionNamedType|ReflectionUnionType|ReflectionIntersectionType|null $reflectionType */
        $reflectionType = $property->getType();
        return $this->resolveType($reflectionType);
    }

    public function resolveString(string $type): Type
    {
        if (class_exists($type)) {
            return new ClassType($type);
        }

        if (interface_exists($type)) {
            return new InterfaceType($type);
        }

        return match ($type) {
            'array' => new ArrayType(),
            'bool' => new BoolType(),
            'float' => new FloatType(),
            'int' => new IntegerType(),
            'string' => new StringType(),
            'iterable' => new IterableType(),
            'mixed' => new MixedType(),
            'object' => new ObjectType(),
            'callable' => CallableType::create(),
            'self' => new SelfType(),
            'static' => new StaticType(),
            default => throw new LogicException("Unknown type {$type}"),
        };
    }

    private function resolveType(ReflectionIntersectionType|ReflectionUnionType|ReflectionNamedType|null $reflectionType, bool $isVariadic = false): Type
    {
        if ($reflectionType === null) {
            return new MixedType();
        }

        $type = match ($reflectionType::class) {
            ReflectionNamedType::class => $this->reflectionTypeToType($reflectionType),
            ReflectionUnionType::class, ReflectionIntersectionType::class => $this->reflectionUnionOrIntersectionTypeToType($reflectionType),
            default => throw new \LogicException(),
        };

        if ($reflectionType->allowsNull()) {
            return new UnionType($type, new NullType());
        }

        if ($isVariadic) {
            return new VariadicType($type);
        }
        return $type;
    }

    private function reflectionTypeToType(ReflectionNamedType $type): Type
    {
        return $this->resolveString($type->getName());
    }

    private function reflectionUnionOrIntersectionTypeToType(ReflectionUnionType|ReflectionIntersectionType $type): Type
    {
        $types = [];
        foreach ($type->getTypes() as $subtype) {
            $types[] = $this->resolveType($subtype);
        }

        return match ($type::class) {
            ReflectionUnionType::class => new UnionType(...$types),
            ReflectionIntersectionType::class => new IntersectionType(...$types),
            default => throw new \LogicException(),
        };
    }
}
