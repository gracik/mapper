<?php

declare(strict_types=1);

namespace Gracik\Mapper\Type;

class InterfaceType implements Type
{
    /**
     * @param class-string $interface
     * @param array<Type> $genericParameterTypes
     */
    public function __construct(
        public readonly string $interface,
        public readonly array $genericParameterTypes = [],
    ) {
    }

    public function toPhpTypeString(): string
    {
        return "\\$this->interface";
    }

    public function toDocblockTypeString(): string
    {
        $genericParameters = $this->genericParameterTypes !== []
            ? '<' . implode(', ', array_map(static fn (Type $type) => $type->toDocblockTypeString(), $this->genericParameterTypes)) . '>'
            : '';
        return "\\{$this->interface}{$genericParameters}";
    }

    public function __toString(): string
    {
        return $this->toDocblockTypeString();
    }
}
