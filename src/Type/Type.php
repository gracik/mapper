<?php

declare(strict_types=1);

namespace Gracik\Mapper\Type;

use Stringable;

interface Type extends Stringable
{
    public function toPhpTypeString(): string;
    public function toDocblockTypeString(): string;
}
