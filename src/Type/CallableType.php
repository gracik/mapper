<?php

declare(strict_types=1);

namespace Gracik\Mapper\Type;

final class CallableType implements Type
{
    /**
     * @param non-empty-list<Type>|null $parameterTypes
     */
    private function __construct(
        public readonly ?array $parameterTypes = null,
        public readonly ?Type $returnType = null,
    ) {
    }

    public static function create(): self
    {
        return new self();
    }

    /**
     * @param non-empty-list<Type> $parameterTypes
     */
    public static function createWithParametersAndReturnType(
        array $parameterTypes,
        Type $returnType,
    ): self {
        return new self($parameterTypes, $returnType);
    }

    public function toPhpTypeString(): string
    {
        return 'callable';
    }

    public function toDocblockTypeString(): string
    {
        if (($this->parameterTypes === null && $this->returnType !== null) || ($this->parameterTypes !== null && $this->returnType === null)) {
            throw new \LogicException('Both parameter and return types must or must not be null');
        }

        if ($this->parameterTypes === null || $this->returnType === null) {
            return 'callable';
        }

        $parameters = implode(', ', array_map(static fn (Type $type) => (string)$type, $this->parameterTypes));
        return "callable($parameters): $this->returnType";
    }

    public function __toString(): string
    {
        return $this->toDocblockTypeString();
    }
}
