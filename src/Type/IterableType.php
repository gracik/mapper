<?php

declare(strict_types=1);

namespace Gracik\Mapper\Type;

final class IterableType implements Type
{
    public function __construct(
        public readonly Type $key = new UnionType(new IntegerType(), new StringType()),
        public readonly Type $value = new MixedType(),
    ) {
    }

    public function toPhpTypeString(): string
    {
        return 'iterable';
    }

    public function toDocblockTypeString(): string
    {
        return "iterable<$this->key, $this->value>";
    }

    public function __toString(): string
    {
        return $this->toDocblockTypeString();
    }
}
