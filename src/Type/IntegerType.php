<?php

declare(strict_types=1);

namespace Gracik\Mapper\Type;

final class IntegerType implements Type
{
    public function toPhpTypeString(): string
    {
        return (string)$this;
    }

    public function toDocblockTypeString(): string
    {
        return (string)$this;
    }

    public function __toString(): string
    {
        return 'int';
    }
}
