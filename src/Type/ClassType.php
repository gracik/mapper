<?php

declare(strict_types=1);

namespace Gracik\Mapper\Type;

class ClassType implements Type
{
    /**
     * @param class-string $class
     * @param array<Type> $genericParameterTypes
     */
    public function __construct(
        public readonly string $class,
        public readonly array $genericParameterTypes = [],
    ) {
    }

    public function getClassName(): string
    {
        $explode = explode('\\', $this->class);
        return end($explode);
    }

    public function toPhpTypeString(): string
    {
        return "\\$this->class";
    }

    public function toDocblockTypeString(): string
    {
        $genericParameters = $this->genericParameterTypes !== []
            ? '<' . implode(', ', array_map(static fn (Type $type) => $type->toDocblockTypeString(), $this->genericParameterTypes)) . '>'
            : '';
        return "\\{$this->class}{$genericParameters}";
    }

    public function __toString(): string
    {
        return $this->toDocblockTypeString();
    }
}
