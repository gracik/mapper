<?php

declare(strict_types=1);

namespace Gracik\Mapper\Type;

class UnionType implements Type
{
    /** @var non-empty-list<Type> */
    public readonly array $types;

    public function __construct(Type $type, Type ...$otherTypes)
    {
        $this->types = [$type, ...array_values($otherTypes)];
    }

    public function toPhpTypeString(): string
    {
        return implode('|', array_map(static fn (Type $type) => $type->toPhpTypeString(), $this->types));
    }

    public function toDocblockTypeString(): string
    {
        return implode('|', array_map(static fn (Type $type) => $type->toDocblockTypeString(), $this->types));
    }

    public function __toString(): string
    {
        return $this->toDocblockTypeString();
    }
}
