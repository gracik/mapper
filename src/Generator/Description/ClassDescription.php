<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Description;

use Gracik\Mapper\Type\ClassType;
use Gracik\Mapper\Type\InterfaceType;

final class ClassDescription
{
    /**
     * @param array<ClassType> $extends
     * @param array<InterfaceType> $implements
     * @param array<MethodDescription> $methods
     */
    public function __construct(
        public readonly string $fqn,
        public readonly array $extends,
        public readonly array $implements,
        public readonly array $methods,
    ) {
    }

    public function namespace(): ?string
    {
        return $this->fqnToNamespaceAndClassName()[0];
    }

    public function className(): string
    {
        return $this->fqnToNamespaceAndClassName()[1];
    }

    /**
     * @return array{string|null, string}
     */
    private function fqnToNamespaceAndClassName(): array
    {
        $fqn = $this->fqn;
        if (str_starts_with($fqn, '\\')) {
            $fqn = mb_substr($fqn, 1);
        }
        $lastSlashIndex = strrpos($fqn, '\\');
        $namespace = null;
        if ($lastSlashIndex !== false) {
            $namespace = mb_substr($fqn, 0, $lastSlashIndex);
        }

        return [$namespace, mb_substr($fqn, $lastSlashIndex + 1)];
    }
}
