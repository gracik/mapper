<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Description;

use Gracik\Mapper\Generator\Code\CodeBlock;
use Gracik\Mapper\Type\Type;

final class MethodDescription
{
    /**
     * @param array<ParameterDescription> $parameters
     */
    public function __construct(
        public readonly string $name,
        public readonly ?Type $returnType,
        public readonly array $parameters,
        public readonly CodeBlock $body,
    ) {
    }
}
