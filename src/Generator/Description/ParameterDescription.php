<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Description;

use Gracik\Mapper\Type\Type;

final class ParameterDescription
{
    public function __construct(
        public readonly string $name,
        public readonly Type $type,
    ) {
    }
}
