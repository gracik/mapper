<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator;

use Gracik\Mapper\Mapper\ClassMapperInterface;
use Gracik\Mapper\Type\BoolType;
use Gracik\Mapper\Type\ClassType;
use Gracik\Mapper\Type\FloatType;
use Gracik\Mapper\Type\IntegerType;
use Gracik\Mapper\Type\InterfaceType;
use Gracik\Mapper\Type\StringType;
use Gracik\Mapper\Type\Type;
use Gracik\Mapper\Type\TypeResolver;
use Gracik\Mapper\Validator\BoolValidatorSnippet;
use Gracik\Mapper\Validator\FloatValidatorSnippet;
use Gracik\Mapper\Validator\IntegerValidatorSnippet;
use Gracik\Mapper\Validator\StringValidatorSnippet;
use InvalidArgumentException;
use PhpParser\BuilderFactory;
use PhpParser\Node;
use PhpParser\Node\Expr\ArrayDimFetch;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter\Standard;
use ReflectionClass;

final readonly class ClassMapperGenerator
{
    public function __construct(
        private TypeResolver $typeResolver,
    ) {
    }

    /**
     * @param class-string $class
     */
    public function generate(string $class, string $mapperClass): string
    {
        $factory = new BuilderFactory();

        if ($mapperClass === '' || str_ends_with($mapperClass, '\\')) {
            throw new InvalidArgumentException("Invalid mapper class");
        }
        if (!str_starts_with($mapperClass, '\\')) {
            $mapperClass = "\\$mapperClass";
        }

        // $mapperClass = '\\' . $mapperNamespace . '\\' . $mapperClassName
        $mapperNamespace = substr($mapperClass, 1, strrpos($mapperClass, '\\') - 1);
        $mapperClassName = substr($mapperClass, 1 + strlen($mapperNamespace) + 1);

        $classType = new ClassType($class);
        $mapperClassMapMethodParameterName = lcfirst($classType->getClassName());

        $methodAst = $factory->method('map')
            ->makePublic()
            ->addParam($factory->param($mapperClassMapMethodParameterName)->setType('array'))
            ->setReturnType('\\' . $class);

        $reflectionClass = new ReflectionClass($class);

        $reflectionConstructor = $reflectionClass->getConstructor();

        $constructorParameters = [];
        if ($reflectionConstructor !== null) {
            foreach ($reflectionConstructor->getParameters() as $parameter) {
                $parameterType = $parameter->getType();
                $parameterName = $parameter->getName();
                if ($parameterType === null) {
                    throw new InvalidArgumentException("$class::__construct parameter \${$parameter->name} must specify its type to be mapped");
                }
                if ($parameterType instanceof \ReflectionUnionType || $parameterType instanceof \ReflectionIntersectionType) {
                    throw new InvalidArgumentException("$class::__construct parameter {$parameterType} \${$parameter->name} is not supported");
                }

                $snippet = $this->getSnippetFor($this->typeResolver->resolveParameter($parameter));
                $snippetReflectionClass = new ReflectionClass($snippet::class);

                $snippetStmts = $this->getSnippetAst($mapperClassMapMethodParameterName, $snippetReflectionClass, $parameterName);


                $lastSnippetStmt = array_pop($snippetStmts);
                if (!($lastSnippetStmt instanceof Stmt\Return_)) {
                    $snippetClass = $snippet::class;
                    throw new InvalidArgumentException("Expected last statement in $snippetClass to be a return.");
                }

                $methodAst->addStmts($snippetStmts);
                $constructorParameters[] = $lastSnippetStmt->expr;
            }
        }

        $methodAst->addStmt(
            new Stmt\Return_(
                $factory->new('\\' . $class, $constructorParameters),
            )
        );

        $genericInterface = new InterfaceType(ClassMapperInterface::class, [$classType]);
        $classAst = $factory->class($mapperClassName)
            ->implement('\\' . ClassMapperInterface::class)
            ->setDocComment("
            /**
             * @extends {$genericInterface->toDocblockTypeString()}
             */")
            ->addStmt($methodAst);

        $fileAst = [
            (new Stmt\Declare_([new Stmt\DeclareDeclare('strict_types', $factory->val(1))])),
            ($mapperNamespace !== '' ?
                $factory->namespace($mapperNamespace)
                    ->addStmt($classAst)
                : $classAst)->getNode(),
        ];

        return (new Standard())->prettyPrintFile($fileAst);
    }

    private function getSnippetFor(Type $type): IntegerValidatorSnippet|StringValidatorSnippet|FloatValidatorSnippet|BoolValidatorSnippet
    {
        return match ($type::class) {
            IntegerType::class => new IntegerValidatorSnippet($type),
            StringType::class => new StringValidatorSnippet($type),
            FloatType::class => new FloatValidatorSnippet($type),
            BoolType::class => new BoolValidatorSnippet($type),
            default => throw new \Exception('Unexpected match value')
        };
    }

    /**
     * @param ReflectionClass<object> $reflectionClass
     * @return array<Node>
     */
    public function getSnippetAst(string $mapperParameterName, ReflectionClass $reflectionClass, string $parameterName): array
    {
        $filename = $reflectionClass->getFileName();
        if ($filename === false) {
            throw new InvalidArgumentException("Could not parse native class {$reflectionClass->getName()}");
        }
        $fileContents = file_get_contents($filename);
        if ($fileContents === false) {
            throw new InvalidArgumentException("Could not read $filename");
        }

        $parser = (new ParserFactory())->create(ParserFactory::ONLY_PHP7);
        $ast = $parser->parse($fileContents);
        if ($ast === null) {
            throw new InvalidArgumentException("Could not parse {$reflectionClass->getName()}");
        }

        /** @var Class_|null $classAst */
        $classAst = null;

        while (true) {
            foreach ($ast as $statement) {
                switch ($statement::class) {
                    case Namespace_::class:
                        $ast = $statement->stmts;
                        continue 3;
                    case Class_::class:
                        $classAst = $statement;
                        break 3;
                    default:
                        continue 2;
                }
            }
        }

        $methodName = '__invoke';
        $methodAst = $classAst?->getMethod($methodName) ?? throw new InvalidArgumentException("Could not find method __invoke of {$reflectionClass->name}");
        $snippetStmts = $methodAst->getStmts() ?? throw new InvalidArgumentException("Could not parse {$reflectionClass->getName()}");

        $reflectionMethod = $reflectionClass->getMethod($methodName);
        $methodParameters = $reflectionMethod->getParameters();
        if (count($methodParameters) !== 1) {
            throw new InvalidArgumentException("Expected one parameter in {$reflectionClass->name}::{$methodName}(), got {$reflectionMethod->getNumberOfParameters()}");
        }
        $methodParameterName = $methodParameters[0]->getName();

        $stmts = $snippetStmts;

        $traverser = new NodeTraverser();
        $traverser->addVisitor(new class($mapperParameterName, $methodParameterName, $parameterName) extends NodeVisitorAbstract {
            private int $returnCounter = 0;

            public function __construct(
                private readonly string $mapperParameterName,
                private readonly string $methodParameterName,
                private readonly string $parameterName,
            ) {
            }

            public function leaveNode(Node $node): ?Node
            {
                if (($node instanceof Stmt\Return_) && ++$this->returnCounter > 1) {
                    throw new InvalidArgumentException('Multiple return statements in validators are currently not supported');
                }
                if ($node instanceof Variable && $node->name === $this->methodParameterName) {
                    return new ArrayDimFetch(new Variable($this->mapperParameterName), new String_($this->parameterName));
                }

                return null;
            }
        });

        return $traverser->traverse($stmts);
    }
}
