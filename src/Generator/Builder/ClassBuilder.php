<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Builder;

use Gracik\Mapper\Generator\Description\ClassDescription;
use Gracik\Mapper\Generator\Description\MethodDescription;
use Gracik\Mapper\Type\ClassType;
use Gracik\Mapper\Type\InterfaceType;

final class ClassBuilder
{
    /**
     * @param array<ClassType> $extends
     * @param array<InterfaceType> $implements
     * @param array<MethodDescription> $methods
     */
    private function __construct(
        private readonly string $fqn,
        private array $extends,
        private array $implements,
        private array $methods,
    ) {
    }

    public static function create(string $name): self
    {
        return new self(
            $name,
            [],
            [],
            [],
        );
    }

    public function extends(ClassType $param): self
    {
        $this->extends[] = $param;
        return $this;
    }

    public function implements(InterfaceType $param): self
    {
        $this->implements[] = $param;
        return $this;
    }


    public function createMethodBuilder(string $name): MethodBuilder
    {
        return MethodBuilder::create($name, $this);
    }

    public function setMethod(MethodDescription $method): void
    {
        $this->methods[$method->name] = $method;
    }

    public function build(): ClassDescription
    {
        return new ClassDescription(
            $this->fqn,
            $this->extends,
            $this->implements,
            $this->methods,
        );
    }
}
