<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Builder;

use Gracik\Mapper\Generator\Code\CodeBlock;
use Gracik\Mapper\Generator\Description\MethodDescription;
use Gracik\Mapper\Generator\Description\ParameterDescription;
use Gracik\Mapper\Type\Type;

final class MethodBuilder
{
    /**
     * @param array<ParameterDescription> $parameters
     */
    private function __construct(
        private readonly ClassBuilder $parent,
        private string $name,
        private ?Type $returnType,
        private array $parameters,
        private CodeBlock $body,
    ) {
    }

    public static function create(string $name, ClassBuilder $parent): self
    {
        return new self(
            $parent,
            $name,
            null,
            [],
            CodeBlock::empty(),
        );
    }

    public function build(): void
    {
        $this->parent->setMethod(
            new MethodDescription(
                $this->name,
                $this->returnType,
                $this->parameters,
                $this->body,
            ),
        );
    }

    public function addParameter(string $name, Type $type): self
    {
        $this->parameters[$name] = new ParameterDescription($name, $type);
        return $this;
    }

    public function setReturnType(Type $returnType): self
    {
        $this->returnType = $returnType;
        return $this;
    }

    public function appendBody(CodeBlock $code): self
    {
        $this->body = $this->body->merge($code);
        return $this;
    }
}
