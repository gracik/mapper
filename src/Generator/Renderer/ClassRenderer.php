<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Renderer;

use Gracik\Mapper\Generator\Description\ClassDescription;
use Gracik\Mapper\Generator\Description\MethodDescription;
use Gracik\Mapper\Type\ClassType;
use Gracik\Mapper\Type\InterfaceType;

final class ClassRenderer
{
    public function __construct(
        private readonly MethodRenderer $methodRenderer,
    ) {
    }

    public function render(ClassDescription $class): string
    {
        $namespace = $class->namespace();
        $className = $class->className();

        $methods = implode(PHP_EOL . PHP_EOL, array_map(fn (MethodDescription $method) => $this->methodRenderer->render($method), $class->methods));

        return
            '<?php' . PHP_EOL .
            PHP_EOL .

            // strict types
            'declare(strict_types=1);' . PHP_EOL .
            PHP_EOL .

            // namespace
            (
                $namespace !== null && $namespace !== ''
                ? (
                    "namespace $namespace;" . PHP_EOL .
                    PHP_EOL
                )
                : ''
            ) .
            // docblock begin
            '/**' . PHP_EOL .

            // docblock extends & implements
            implode('', array_map(fn (ClassType $c) => " * @extends {$c->toDocblockTypeString()}" . PHP_EOL, $class->extends)) .
            implode('', array_map(fn (InterfaceType $i) => " * @implements {$i->toDocblockTypeString()}" . PHP_EOL, $class->implements)) .

            ' */' . PHP_EOL .
            // docblock end

            // class begin
            "final class $className" .
            ($class->extends !== [] ? ' extends ' . implode(',', array_map(fn (ClassType $c) => $c->toPhpTypeString(), $class->extends)) : '') .
            ($class->implements !== [] ? ' implements ' . implode(',', array_map(fn (InterfaceType $i) => $i->toPhpTypeString(), $class->implements)) : '') .
            ' {' . PHP_EOL .

            Indentator::indent(
                $methods,
            ) . PHP_EOL .

            "}";
        // class end
    }
}
