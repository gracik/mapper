<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Renderer;

final class Indentator
{
    public static function indent(string $code, int $count = 1, string $indentWith = '    ', bool $skipFirstLine = false): string
    {
        $indent = str_repeat($indentWith, $count);

        return (!$skipFirstLine ? $indent : '') . str_replace(PHP_EOL, PHP_EOL . $indent, $code);
    }
}
