<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Renderer;

use Gracik\Mapper\Generator\Description\MethodDescription;
use Gracik\Mapper\Generator\Description\ParameterDescription;

final class MethodRenderer
{
    public function render(MethodDescription $method): string
    {
        $visibilityModifier = 'public';

        return
            // docblock begin
            '/**' . PHP_EOL .

            // docblock params
            implode(', ', array_map(fn (ParameterDescription $p) => " * @param {$p->type->toDocblockTypeString()} \${$p->name}" . PHP_EOL, $method->parameters)) .

            // docblock return value
            (
                $method->returnType !== null
                ? (
                    " * @return {$method->returnType->toDocblockTypeString()}" . PHP_EOL
                )
                : ''
            ) .
            ' */' . PHP_EOL .
            // docblock end

            // method begin
            "$visibilityModifier function $method->name" .

            // parameters
            '(' .
            implode(', ', array_map(fn (ParameterDescription $p) => "{$p->type->toPhpTypeString()} \${$p->name}", $method->parameters)) .
            ')' .

            // return type
            ': ' . ($method->returnType?->toPhpTypeString() ?? 'void') . PHP_EOL .

            // body
            '{' . PHP_EOL .
            Indentator::indent($method->body->body) . PHP_EOL .
            '}';
        // method end
    }
}
