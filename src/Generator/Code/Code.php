<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Code;

use Gracik\Mapper\Type\MixedType;

abstract class Code
{
    /** @var array<Variable> */
    private array $helperVariables = [];

    /**
     * @return array<Variable>
     */
    abstract public function getInput(): array;

    /**
     * @return array<Variable|ReturnOutput>
     */
    abstract public function getOutput(): array;

    /**
     * @return array<Variable>
     */
    public function getDeclaredVariables(): array
    {
        return $this->helperVariables;
    }

    public function useHelperVariable(string $name, mixed $value = null): void
    {
        $this->helperVariables[$name] = new Variable($name, new MixedType());
    }
}
