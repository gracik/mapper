<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Code;

final class CodeBlock
{
    public function __construct(
        public readonly string $body,
    ) {
    }

    public static function empty(): self
    {
        return new self('');
    }

    public function merge(CodeBlock $other): self
    {
        $nonEmptyBodies = array_filter([$this->body, $other->body], fn (string $body) => $body !== '');
        return new CodeBlock(
            implode(PHP_EOL, $nonEmptyBodies),
        );
    }
}
