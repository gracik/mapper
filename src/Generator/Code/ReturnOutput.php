<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Code;

use Gracik\Mapper\Type\Type;

final readonly class ReturnOutput
{
    public function __construct(
        public Type $type,
    ) {
    }
}
