<?php

declare(strict_types=1);

namespace Gracik\Mapper\Generator\Code;

use Gracik\Mapper\Type\Type;

final readonly class Variable
{
    public function __construct(
        public string $name,
        public Type $type,
    ) {
    }
}
