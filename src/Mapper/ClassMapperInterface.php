<?php

declare(strict_types=1);

namespace Gracik\Mapper\Mapper;

/**
 * @template T of object
 */
interface ClassMapperInterface
{
    /**
     * @param array<string,mixed> $value
     * @return T
     */
    public function map(array $value): object;
}
