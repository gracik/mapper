<?php

declare(strict_types=1);

namespace Gracik\Mapper\Validator;

use Gracik\Mapper\Type\IntegerType;

/**
 * @extends ValidatorSnippet<int>
 */
class IntegerValidatorSnippet extends ValidatorSnippet
{
    public function __construct(IntegerType $type)
    {
        parent::__construct($type);
    }

    public function __invoke(mixed $value): int
    {
        if (!is_int($value)) {
            throw ValidatorException::invalidType('int', $value);
        }

        return $value;
    }
}
