<?php

declare(strict_types=1);

namespace Gracik\Mapper\Validator;

use Gracik\Mapper\Type\Type;

/**
 * @template T
 */
abstract class ValidatorSnippet
{
    public function __construct(public readonly Type $type)
    {
    }

    /**
     * @return T
     */
    abstract public function __invoke(mixed $value): mixed;
}
