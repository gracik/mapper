<?php

declare(strict_types=1);

namespace Gracik\Mapper\Validator;

use RuntimeException;

final class ValidatorException extends RuntimeException
{
    public static function invalidType(string $expectedType, mixed $actualValue): self
    {
        $actualType = get_debug_type($actualValue);
        return new self("Expected '$expectedType', got '$actualType'");
    }
}
