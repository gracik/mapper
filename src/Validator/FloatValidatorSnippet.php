<?php

declare(strict_types=1);

namespace Gracik\Mapper\Validator;

use Gracik\Mapper\Type\FloatType;

/**
 * @extends ValidatorSnippet<float|int>
 */
class FloatValidatorSnippet extends ValidatorSnippet
{
    public function __construct(FloatType $type)
    {
        parent::__construct($type);
    }

    public function __invoke(mixed $value): float
    {
        if (!is_float($value) && !is_int($value)) {
            throw ValidatorException::invalidType('float', $value);
        }

        return (float)$value;
    }
}
