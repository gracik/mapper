<?php

declare(strict_types=1);

namespace Gracik\Mapper\Validator;

use Gracik\Mapper\Type\BoolType;

/**
 * @extends ValidatorSnippet<bool>
 */
class BoolValidatorSnippet extends ValidatorSnippet
{
    public function __construct(BoolType $type)
    {
        parent::__construct($type);
    }

    public function __invoke(mixed $value): bool
    {
        if (!is_bool($value)) {
            throw ValidatorException::invalidType('bool', $value);
        }

        return $value;
    }
}
