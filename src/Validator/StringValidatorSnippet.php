<?php

declare(strict_types=1);

namespace Gracik\Mapper\Validator;

use Gracik\Mapper\Type\StringType;

/**
 * @extends ValidatorSnippet<string>
 */
class StringValidatorSnippet extends ValidatorSnippet
{
    public function __construct(StringType $type)
    {
        parent::__construct($type);
    }

    public function __invoke(mixed $value): string
    {
        if (!is_string($value)) {
            throw ValidatorException::invalidType('string', $value);
        }

        return $value;
    }
}
